import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule }    from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// import { TransferHttpCacheModule } from '@nguniversal/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//Rutas
import { AppRoutingModule }        from './app-routing.module';
//Servicios
import { AboutService } from './servicios/about.service';

//componentes
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { BodyComponent } from './components/body/body.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ExpositoresComponent } from './expositores/expositores.component';
import { VisitantesComponent } from './visitantes/visitantes.component';
import { VentasComponent } from './ventas/ventas.component';
import { ContactoComponent } from './contacto/contacto.component';
import { GdlComponent } from './gdl/gdl.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    HomeComponent,
    AboutComponent,
    ExpositoresComponent,
    VisitantesComponent,
    VentasComponent,
    ContactoComponent,
    GdlComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
 	  AppRoutingModule
  ],
  providers: [
  AboutService,
  { provide: LOCALE_ID, useValue: 'es' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 

}
