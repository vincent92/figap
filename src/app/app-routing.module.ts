import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent }         from './home/home.component';
import { ExpositoresComponent }  from './expositores/expositores.component';
import { VisitantesComponent }   from './visitantes/visitantes.component';
import { GdlComponent }          from './gdl/gdl.component';
import { AboutComponent }        from './about/about.component';
import { VentasComponent }       from './ventas/ventas.component';
import { ContactoComponent }     from './contacto/contacto.component';

const APPROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'expositores', component: ExpositoresComponent },
  { path: 'visitantes', component: VisitantesComponent },
  { path: 'gdl', component: GdlComponent },
  { path: 'about', component: AboutComponent },
  { path: 'ventas', component: VentasComponent },
  { path: 'contacto', component: ContactoComponent },
  { path: '**', pathMatch: 'full', redirectTo:'home'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      APPROUTES,
      {
        enableTracing: false, // <-- debugging purposes only
        // preloadingStrategy: SelectivePreloadingStrategyService,
        // useHash: true
      }
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }