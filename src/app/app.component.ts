import { Component, LOCALE_ID, Inject } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'figap';
  languages = [
    { code: 'en', label: 'English'},
    { code: 'es', label: 'Español'}
  ];
  constructor(@Inject(LOCALE_ID) protected localeId: string) {}

}
