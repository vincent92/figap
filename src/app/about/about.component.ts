import { Component, OnInit } from '@angular/core';
import { AboutService } from '../servicios/about.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {

  constructor( private _aboutService:AboutService) { }

  ngOnInit() {
  }

}
